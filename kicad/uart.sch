EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4550 4250 2    50   Output ~ 0
uart_rx
Text HLabel 4150 4250 0    50   Input ~ 0
uart_tx
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 604CB820
P 4300 4550
F 0 "J3" V 4454 4362 50  0000 R CNN
F 1 "Conn_01x02_Male" V 4363 4362 50  0000 R CNN
F 2 "" H 4300 4550 50  0001 C CNN
F 3 "~" H 4300 4550 50  0001 C CNN
	1    4300 4550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4300 4350 4300 4250
Wire Wire Line
	4300 4250 4150 4250
Wire Wire Line
	4400 4350 4400 4250
Wire Wire Line
	4400 4250 4550 4250
$EndSCHEMATC
