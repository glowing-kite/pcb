EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4000 4200 0    50   Output ~ 0
uart_rx
Text HLabel 4000 4300 0    50   Input ~ 0
uart_tx
Wire Wire Line
	4150 4300 4000 4300
Wire Wire Line
	4150 4200 4000 4200
Text HLabel 4050 4100 0    50   BiDi ~ 0
swdio
Text HLabel 4050 4000 0    50   Output ~ 0
swclk
Wire Wire Line
	4150 4100 4050 4100
Wire Wire Line
	4150 4000 4050 4000
$Comp
L Connector:Conn_01x06_Male J3
U 1 1 6044BCCA
P 4350 4200
F 0 "J3" V 4504 3812 50  0000 R CNN
F 1 "Conn_01x06_Male" V 4413 3812 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 4350 4200 50  0001 C CNN
F 3 "~" H 4350 4200 50  0001 C CNN
	1    4350 4200
	-1   0    0    1   
$EndComp
Wire Wire Line
	4150 3900 3500 3900
Wire Wire Line
	3500 3900 3500 4000
Wire Wire Line
	4150 4400 3500 4400
Wire Wire Line
	3500 4400 3500 4500
$Comp
L power:GND #PWR0155
U 1 1 6044FDA6
P 3500 4500
F 0 "#PWR0155" H 3500 4250 50  0001 C CNN
F 1 "GND" H 3505 4327 50  0000 C CNN
F 2 "" H 3500 4500 50  0001 C CNN
F 3 "" H 3500 4500 50  0001 C CNN
	1    3500 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0156
U 1 1 60450120
P 3500 4000
F 0 "#PWR0156" H 3500 3750 50  0001 C CNN
F 1 "GND" H 3505 3827 50  0000 C CNN
F 2 "" H 3500 4000 50  0001 C CNN
F 3 "" H 3500 4000 50  0001 C CNN
	1    3500 4000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
