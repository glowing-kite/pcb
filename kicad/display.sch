EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR0103
U 1 1 60391463
P 3450 3050
F 0 "#PWR0103" H 3450 2800 50  0001 C CNN
F 1 "GND" H 3450 2900 50  0000 C CNN
F 2 "" H 3450 3050 50  0001 C CNN
F 3 "" H 3450 3050 50  0001 C CNN
	1    3450 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 2900 4500 2900
Wire Wire Line
	4500 2900 4500 2750
Wire Wire Line
	3450 3000 3450 3050
Text HLabel 4400 3100 0    50   Input ~ 0
display_nss
Wire Wire Line
	3450 3000 4650 3000
Text HLabel 4400 3200 0    50   Input ~ 0
display_reset
Text HLabel 4400 3300 0    50   Input ~ 0
display_dc
Text HLabel 4400 3400 0    50   Input ~ 0
display_mosi
Text HLabel 4400 3500 0    50   Input ~ 0
display_sck
Text HLabel 4400 3600 0    50   Input ~ 0
display_led
Text HLabel 4400 3700 0    50   Output ~ 0
display_miso
Wire Wire Line
	4400 3100 4650 3100
Wire Wire Line
	4650 3200 4400 3200
Wire Wire Line
	4400 3300 4650 3300
Wire Wire Line
	4650 3400 4400 3400
Wire Wire Line
	4400 3500 4650 3500
Wire Wire Line
	4400 3600 4650 3600
Wire Wire Line
	4650 3700 4400 3700
$Comp
L Connector:Conn_01x09_Female J1
U 1 1 6040DBAE
P 4850 3300
F 0 "J1" H 4878 3326 50  0000 L CNN
F 1 "Conn_01x09_Female" H 4878 3235 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x09_P2.54mm_Vertical" H 4850 3300 50  0001 C CNN
F 3 "~" H 4850 3300 50  0001 C CNN
	1    4850 3300
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0102
U 1 1 60417593
P 4500 2750
F 0 "#PWR0102" H 4500 2600 50  0001 C CNN
F 1 "+3.3V" H 4515 2923 50  0000 C CNN
F 2 "" H 4500 2750 50  0001 C CNN
F 3 "" H 4500 2750 50  0001 C CNN
	1    4500 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J2
U 1 1 603FA227
P 4600 4550
F 0 "J2" H 4628 4526 50  0000 L CNN
F 1 "Conn_01x02_Female" H 4628 4435 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4600 4550 50  0001 C CNN
F 3 "~" H 4600 4550 50  0001 C CNN
	1    4600 4550
	1    0    0    -1  
$EndComp
$EndSCHEMATC
