EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6600 2350 6850 2350
Text GLabel 6850 2350 2    50   Output ~ 0
enc_nss
Wire Wire Line
	6600 2450 6850 2450
Wire Wire Line
	6600 2550 6850 2550
Wire Wire Line
	6600 2650 6850 2650
Text GLabel 6850 2450 2    50   Output ~ 0
enc_sck
Text GLabel 6850 2550 2    50   Input ~ 0
enc_miso
Text GLabel 6850 2650 2    50   Output ~ 0
enc_mosi
Wire Wire Line
	6600 2750 6850 2750
Text GLabel 6850 2750 2    50   Input ~ 0
enc_int
$Sheet
S 9150 2450 550  850 
U 6038B576
F0 "enc28j60" 50
F1 "enc28j60.sch" 50
F2 "enc_int" O L 9150 2950 50 
F3 "enc_miso" O L 9150 2750 50 
F4 "enc_nss" I L 9150 2550 50 
F5 "enc_sck" I L 9150 2650 50 
F6 "enc_mosi" I L 9150 2850 50 
F7 "enc_wol" O L 9150 3200 50 
F8 "enc_reset" O L 9150 3100 50 
$EndSheet
Wire Wire Line
	9150 2550 8900 2550
Text GLabel 8900 2550 0    50   Input ~ 0
enc_nss
Wire Wire Line
	9150 2650 8900 2650
Wire Wire Line
	9150 2750 8900 2750
Wire Wire Line
	9150 2850 8900 2850
Text GLabel 8900 2650 0    50   Input ~ 0
enc_sck
Text GLabel 8900 2750 0    50   Output ~ 0
enc_miso
Text GLabel 8900 2850 0    50   Input ~ 0
enc_mosi
Wire Wire Line
	9150 2950 8900 2950
Text GLabel 8900 2950 0    50   Output ~ 0
enc_int
$Sheet
S 9000 4450 750  900 
U 6038ECDE
F0 "display" 50
F1 "display.sch" 50
F2 "display_nss" I L 9000 4500 50 
F3 "display_reset" I L 9000 5250 50 
F4 "display_dc" I L 9000 5150 50 
F5 "display_mosi" I L 9000 4800 50 
F6 "display_sck" I L 9000 4600 50 
F7 "display_led" I L 9000 5050 50 
F8 "display_miso" O L 9000 4700 50 
$EndSheet
Text GLabel 7000 5150 2    50   Output ~ 0
display_mosi
Text GLabel 7000 5050 2    50   Input ~ 0
display_miso
Text GLabel 7000 4950 2    50   Output ~ 0
display_sck
Text GLabel 7000 4850 2    50   Output ~ 0
display_nss
Wire Wire Line
	6600 4850 7000 4850
Wire Wire Line
	6600 4950 7000 4950
Wire Wire Line
	7000 5050 6600 5050
Wire Wire Line
	6600 5150 7000 5150
Text GLabel 8600 4800 0    50   Input ~ 0
display_mosi
Text GLabel 8600 4700 0    50   Output ~ 0
display_miso
Text GLabel 8600 4600 0    50   Input ~ 0
display_sck
Text GLabel 8600 4500 0    50   Input ~ 0
display_nss
Wire Wire Line
	9000 4500 8600 4500
Wire Wire Line
	9000 4600 8600 4600
Wire Wire Line
	8600 4700 9000 4700
Wire Wire Line
	9000 4800 8600 4800
$Sheet
S 2500 4500 550  550 
U 6039A012
F0 "buttons" 50
F1 "buttons.sch" 50
F2 "button_0" I R 3050 4550 50 
F3 "button_1" I R 3050 4650 50 
F4 "button_2" I R 3050 4750 50 
F5 "button_3" I R 3050 4850 50 
F6 "button_4" I R 3050 4950 50 
$EndSheet
$Sheet
S 9100 1600 550  450 
U 6039A132
F0 "led_buzzer" 50
F1 "led_buzzer.sch" 50
F2 "led_1" I L 9100 1800 50 
F3 "buzzer" I L 9100 1900 50 
F4 "led_0" I L 9100 1700 50 
F5 "12V_LED" I L 9100 2000 50 
$EndSheet
$Sheet
S 2500 2950 550  350 
U 6039DD45
F0 "power_supply" 50
F1 "power_supply.sch" 50
F2 "3.3V" O R 3050 3000 50 
F3 "VBAT" O R 3050 3100 50 
F4 "12V_LED" O R 3050 3200 50 
$EndSheet
Wire Wire Line
	5800 5350 5800 5400
Wire Wire Line
	5900 5350 5900 5400
Wire Wire Line
	6000 5350 6000 5400
Wire Wire Line
	6000 5400 5900 5400
Wire Wire Line
	5900 5400 5800 5400
Connection ~ 5900 5400
Wire Wire Line
	5900 5400 5900 5450
$Comp
L power:GND #PWR0101
U 1 1 603A2412
P 5900 5450
F 0 "#PWR0101" H 5900 5200 50  0001 C CNN
F 1 "GND" H 5905 5277 50  0000 C CNN
F 2 "" H 5900 5450 50  0001 C CNN
F 3 "" H 5900 5450 50  0001 C CNN
	1    5900 5450
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0104
U 1 1 603A2966
P 6100 1550
F 0 "#PWR0104" H 6100 1400 50  0001 C CNN
F 1 "+3.3V" H 6115 1723 50  0000 C CNN
F 2 "" H 6100 1550 50  0001 C CNN
F 3 "" H 6100 1550 50  0001 C CNN
	1    6100 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1750 5800 1650
Wire Wire Line
	5900 1750 5900 1650
Wire Wire Line
	6000 1750 6000 1650
Wire Wire Line
	6100 1750 6100 1650
Wire Wire Line
	6200 1750 6200 1650
Wire Wire Line
	6200 1650 6100 1650
Connection ~ 5900 1650
Wire Wire Line
	5900 1650 5800 1650
Connection ~ 6000 1650
Wire Wire Line
	6000 1650 5900 1650
Connection ~ 6100 1650
Wire Wire Line
	6100 1650 6000 1650
Wire Wire Line
	6100 1650 6100 1550
$Comp
L power:+BATT #PWR0105
U 1 1 603A88FE
P 5700 1550
F 0 "#PWR0105" H 5700 1400 50  0001 C CNN
F 1 "+BATT" H 5715 1723 50  0000 C CNN
F 2 "" H 5700 1550 50  0001 C CNN
F 3 "" H 5700 1550 50  0001 C CNN
	1    5700 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 1750 5700 1550
$Comp
L Device:C C3
U 1 1 603AA700
P 4950 2650
F 0 "C3" H 5065 2696 50  0000 L CNN
F 1 "2.2uF" H 5065 2605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4988 2500 50  0001 C CNN
F 3 "~" H 4950 2650 50  0001 C CNN
	1    4950 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 603AAF3E
P 4550 2650
F 0 "C2" H 4665 2696 50  0000 L CNN
F 1 "2.2uF" H 4665 2605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4588 2500 50  0001 C CNN
F 3 "~" H 4550 2650 50  0001 C CNN
	1    4550 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 2500 4950 2450
Wire Wire Line
	4950 2450 5200 2450
Wire Wire Line
	4950 2850 4950 2800
Wire Wire Line
	4950 2850 4950 2900
Connection ~ 4950 2850
$Comp
L power:GND #PWR0106
U 1 1 603AF2DD
P 4950 2900
F 0 "#PWR0106" H 4950 2650 50  0001 C CNN
F 1 "GND" H 5100 2800 50  0000 C CNN
F 2 "" H 4950 2900 50  0001 C CNN
F 3 "" H 4950 2900 50  0001 C CNN
	1    4950 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 2500 4550 2350
Wire Wire Line
	4550 2350 5200 2350
Wire Wire Line
	4550 2850 4550 2800
Wire Wire Line
	4550 2850 4950 2850
Wire Wire Line
	4250 2150 4250 2450
$Comp
L power:GND #PWR0107
U 1 1 603B33DA
P 4250 2450
F 0 "#PWR0107" H 4250 2200 50  0001 C CNN
F 1 "GND" H 4255 2277 50  0000 C CNN
F 2 "" H 4250 2450 50  0001 C CNN
F 3 "" H 4250 2450 50  0001 C CNN
	1    4250 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 3250 6900 3250
Wire Wire Line
	6600 3350 6900 3350
Text GLabel 6900 3250 2    50   Input ~ 0
swdio
Text GLabel 6900 3350 2    50   Input ~ 0
swclk
Wire Wire Line
	6600 4750 7000 4750
Wire Wire Line
	6600 4650 7000 4650
Wire Wire Line
	6600 4550 7000 4550
Text GLabel 7000 4550 2    50   Output ~ 0
display_led
Text GLabel 7000 4650 2    50   Output ~ 0
display_dc
Text GLabel 7000 4750 2    50   Output ~ 0
display_reset
Wire Wire Line
	9000 5050 8600 5050
Wire Wire Line
	9000 5150 8600 5150
Wire Wire Line
	9000 5250 8600 5250
Text GLabel 8600 5050 0    50   Input ~ 0
display_led
Text GLabel 8600 5150 0    50   Input ~ 0
display_dc
Text GLabel 8600 5250 0    50   Input ~ 0
display_reset
Wire Wire Line
	9200 3900 9000 3900
Wire Wire Line
	9200 4000 9000 4000
Text GLabel 9000 3900 0    50   Input ~ 0
swdio
Text GLabel 9000 4000 0    50   Input ~ 0
swclk
Wire Wire Line
	6600 2850 6850 2850
Wire Wire Line
	6600 2950 6850 2950
Wire Wire Line
	9200 3700 9050 3700
Wire Wire Line
	9200 3800 9050 3800
Text GLabel 6850 2850 2    50   Output ~ 0
uart_tx
Text GLabel 6850 2950 2    50   Input ~ 0
uart_rx
Text GLabel 9050 3700 0    50   Input ~ 0
uart_tx
Text GLabel 9050 3800 0    50   Output ~ 0
uart_rx
Text GLabel 6750 2050 2    50   Output ~ 0
led_0
Text GLabel 6750 2150 2    50   Output ~ 0
led_1
Text GLabel 6750 2250 2    50   Output ~ 0
buzzer
Wire Wire Line
	6600 2050 6750 2050
Wire Wire Line
	6600 2150 6750 2150
Wire Wire Line
	6600 2250 6750 2250
Text GLabel 8800 1700 0    50   Input ~ 0
led_0
Text GLabel 8800 1800 0    50   Input ~ 0
led_1
Text GLabel 8800 1900 0    50   Input ~ 0
buzzer
Wire Wire Line
	8800 1700 9100 1700
Wire Wire Line
	9100 1800 8800 1800
Wire Wire Line
	8800 1900 9100 1900
Text GLabel 5050 3650 0    50   Input ~ 0
button_0
Text GLabel 5050 3750 0    50   Input ~ 0
button_1
Text GLabel 5050 3850 0    50   Input ~ 0
button_2
Text GLabel 5050 3950 0    50   Input ~ 0
button_3
Text GLabel 5000 4050 0    50   Input ~ 0
button_4
Wire Wire Line
	5050 3650 5200 3650
Wire Wire Line
	5050 3750 5200 3750
Wire Wire Line
	5200 3850 5050 3850
Wire Wire Line
	5050 3950 5200 3950
Wire Wire Line
	5000 4050 5150 4050
Text GLabel 3250 4550 2    50   Output ~ 0
button_0
Text GLabel 3250 4650 2    50   Output ~ 0
button_1
Text GLabel 3250 4750 2    50   Output ~ 0
button_2
Text GLabel 3250 4850 2    50   Output ~ 0
button_3
Text GLabel 3250 4950 2    50   Output ~ 0
button_4
Wire Wire Line
	3050 4550 3250 4550
Wire Wire Line
	3250 4650 3050 4650
Wire Wire Line
	3050 4750 3250 4750
Wire Wire Line
	3250 4850 3050 4850
Wire Wire Line
	3050 4950 3250 4950
Wire Wire Line
	5200 2150 4250 2150
$Comp
L MCU_ST_STM32F4:STM32F405RGTx U1
U 1 1 6037D113
P 5900 3550
F 0 "U1" H 6550 1700 50  0000 C CNN
F 1 "STM32F405RGTx" H 6650 1600 50  0000 C CNN
F 2 "Package_QFP:LQFP-64_10x10mm_P0.5mm" H 5300 1850 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00037051.pdf" H 5900 3550 50  0001 C CNN
	1    5900 3550
	1    0    0    -1  
$EndComp
Text GLabel 5000 5050 0    50   Input ~ 0
rtc_in
Text GLabel 5000 5150 0    50   Output ~ 0
rtc_out
Wire Wire Line
	5000 5050 5200 5050
Wire Wire Line
	5200 5150 5000 5150
Text GLabel 2950 3850 2    50   Output ~ 0
rtc_in
Text GLabel 2950 3950 2    50   Input ~ 0
rtc_out
Wire Wire Line
	2850 3850 2950 3850
Wire Wire Line
	2950 3950 2850 3950
Text GLabel 5000 3150 0    50   Input ~ 0
main_clk_in
Text GLabel 5000 3250 0    50   Output ~ 0
main_clk_out
Text GLabel 3000 3650 2    50   Output ~ 0
main_clk_in
Text GLabel 3000 3750 2    50   Input ~ 0
main_clk_out
Wire Wire Line
	3000 3650 2850 3650
Wire Wire Line
	3000 3750 2850 3750
Wire Wire Line
	5200 3150 5000 3150
Wire Wire Line
	5200 3250 5000 3250
$Comp
L power:+3.3V #PWR0108
U 1 1 60472C0B
P 3350 2850
F 0 "#PWR0108" H 3350 2700 50  0001 C CNN
F 1 "+3.3V" H 3365 3023 50  0000 C CNN
F 2 "" H 3350 2850 50  0001 C CNN
F 3 "" H 3350 2850 50  0001 C CNN
	1    3350 2850
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR0109
U 1 1 60473232
P 3600 2850
F 0 "#PWR0109" H 3600 2700 50  0001 C CNN
F 1 "+BATT" H 3615 3023 50  0000 C CNN
F 2 "" H 3600 2850 50  0001 C CNN
F 3 "" H 3600 2850 50  0001 C CNN
	1    3600 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 3000 3350 3000
Wire Wire Line
	3350 3000 3350 2850
Wire Wire Line
	3600 3100 3600 2850
Wire Wire Line
	3050 3100 3600 3100
$Comp
L Switch:SW_Push SW1
U 1 1 6048047C
P 2950 1750
F 0 "SW1" H 2950 2035 50  0000 C CNN
F 1 "SW_Push" H 2950 1944 50  0000 C CNN
F 2 "Button_Switch_THT:SW_Tactile_SKHH_Angled" H 2950 1950 50  0001 C CNN
F 3 "~" H 2950 1950 50  0001 C CNN
	1    2950 1750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 6048B379
P 2550 2000
F 0 "#PWR0110" H 2550 1750 50  0001 C CNN
F 1 "GND" H 2555 1827 50  0000 C CNN
F 2 "" H 2550 2000 50  0001 C CNN
F 3 "" H 2550 2000 50  0001 C CNN
	1    2550 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0111
U 1 1 6048B67B
P 3550 1200
F 0 "#PWR0111" H 3550 1050 50  0001 C CNN
F 1 "+3.3V" H 3565 1373 50  0000 C CNN
F 2 "" H 3550 1200 50  0001 C CNN
F 3 "" H 3550 1200 50  0001 C CNN
	1    3550 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 6048C1AA
P 2950 2000
F 0 "C1" V 2700 2000 50  0000 C CNN
F 1 "100nF" V 2800 2000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2988 1850 50  0001 C CNN
F 3 "~" H 2950 2000 50  0001 C CNN
	1    2950 2000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 6048EAB3
P 3550 1500
F 0 "R1" H 3620 1546 50  0000 L CNN
F 1 "100K" H 3620 1455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3480 1500 50  0001 C CNN
F 3 "~" H 3550 1500 50  0001 C CNN
	1    3550 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 2000 2550 2000
Wire Wire Line
	2750 1750 2550 1750
Wire Wire Line
	2550 1750 2550 2000
Connection ~ 2550 2000
Wire Wire Line
	3150 1750 3550 1750
Wire Wire Line
	3550 1750 3550 1650
Wire Wire Line
	3100 2000 3550 2000
Wire Wire Line
	3550 2000 3550 1750
Connection ~ 3550 1750
Wire Wire Line
	3550 1350 3550 1200
Text GLabel 3650 2000 2    50   Output ~ 0
NRST
Text GLabel 5000 1950 0    50   Input ~ 0
NRST
Wire Wire Line
	5000 1950 5200 1950
Wire Wire Line
	3650 2000 3550 2000
Connection ~ 3550 2000
Text GLabel 6850 3050 2    50   Output ~ 0
enc_reset
Text GLabel 6950 3150 2    50   Output ~ 0
enc_wol
Text GLabel 8900 3100 0    50   Input ~ 0
enc_reset
Text GLabel 8900 3200 0    50   Input ~ 0
enc_wol
Wire Wire Line
	9150 3100 8900 3100
Wire Wire Line
	8900 3200 9150 3200
Wire Wire Line
	6850 3050 6600 3050
Wire Wire Line
	6700 3150 6950 3150
$Sheet
S 9200 3650 500  400 
U 604446C9
F0 "uart_swd" 50
F1 "uart_swd.sch" 50
F2 "uart_rx" O L 9200 3800 50 
F3 "uart_tx" I L 9200 3700 50 
F4 "swdio" B L 9200 3900 50 
F5 "swclk" O L 9200 4000 50 
$EndSheet
$Sheet
S 2050 3600 800  450 
U 6044242D
F0 "oscilators" 50
F1 "oscilators.sch" 50
F2 "8Mhz_osc_in" O R 2850 3650 50 
F3 "8Mhz_osc_out" I R 2850 3750 50 
F4 "32.768Hz_osc_in" O R 2850 3850 50 
F5 "32.768Hz_osc_out" I R 2850 3950 50 
$EndSheet
Wire Wire Line
	3050 3200 3400 3200
Text GLabel 3400 3200 2    50   Output ~ 0
12V_LED
Text GLabel 8800 2000 0    50   Input ~ 0
12V_LED
Wire Wire Line
	8800 2000 9100 2000
$EndSCHEMATC
