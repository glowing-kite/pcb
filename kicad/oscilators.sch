EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Crystal Y?
U 1 1 604437CC
P 4750 4000
AR Path="/604437CC" Ref="Y?"  Part="1" 
AR Path="/60440CE4/604437CC" Ref="Y?"  Part="1" 
AR Path="/6044242D/604437CC" Ref="Y3"  Part="1" 
F 0 "Y3" V 4796 3869 50  0000 R CNN
F 1 "8Mhz" V 4705 3869 50  0000 R CNN
F 2 "Crystal:Crystal_HC18-U_Vertical" H 4750 4000 50  0001 C CNN
F 3 "~" H 4750 4000 50  0001 C CNN
	1    4750 4000
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 604437D2
P 5100 4150
AR Path="/604437D2" Ref="C?"  Part="1" 
AR Path="/60440CE4/604437D2" Ref="C?"  Part="1" 
AR Path="/6044242D/604437D2" Ref="C22"  Part="1" 
F 0 "C22" V 4848 4150 50  0000 C CNN
F 1 "20pF" V 4939 4150 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5138 4000 50  0001 C CNN
F 3 "~" H 5100 4150 50  0001 C CNN
	1    5100 4150
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 604437D8
P 5100 3800
AR Path="/604437D8" Ref="C?"  Part="1" 
AR Path="/60440CE4/604437D8" Ref="C?"  Part="1" 
AR Path="/6044242D/604437D8" Ref="C21"  Part="1" 
F 0 "C21" V 5352 3800 50  0000 C CNN
F 1 "20pF" V 5261 3800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5138 3650 50  0001 C CNN
F 3 "~" H 5100 3800 50  0001 C CNN
	1    5100 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4950 4150 4750 4150
Wire Wire Line
	4950 3800 4750 3800
Wire Wire Line
	4750 3800 4750 3850
Wire Wire Line
	3550 4050 4500 4050
Wire Wire Line
	4500 4050 4500 4150
Wire Wire Line
	4500 4150 4750 4150
Connection ~ 4750 4150
Wire Wire Line
	4750 3800 4500 3800
Wire Wire Line
	4500 3800 4500 3950
Connection ~ 4750 3800
Wire Wire Line
	4500 3950 3550 3950
Wire Wire Line
	5250 4150 5250 3950
Wire Wire Line
	5450 3950 5250 3950
Connection ~ 5250 3950
Wire Wire Line
	5250 3950 5250 3800
Wire Wire Line
	5450 3950 5450 3900
$Comp
L power:GND #PWR?
U 1 1 604437EE
P 5450 3900
AR Path="/604437EE" Ref="#PWR?"  Part="1" 
AR Path="/60440CE4/604437EE" Ref="#PWR?"  Part="1" 
AR Path="/6044242D/604437EE" Ref="#PWR0153"  Part="1" 
F 0 "#PWR0153" H 5450 3650 50  0001 C CNN
F 1 "GND" H 5455 3727 50  0000 C CNN
F 2 "" H 5450 3900 50  0001 C CNN
F 3 "" H 5450 3900 50  0001 C CNN
	1    5450 3900
	-1   0    0    1   
$EndComp
Text HLabel 3550 3950 0    50   Output ~ 0
8Mhz_osc_in
Text HLabel 3550 4050 0    50   Input ~ 0
8Mhz_osc_out
Wire Wire Line
	5400 5200 5550 5200
$Comp
L power:GND #PWR?
U 1 1 604458C7
P 5550 5250
AR Path="/604458C7" Ref="#PWR?"  Part="1" 
AR Path="/60440B15/604458C7" Ref="#PWR?"  Part="1" 
AR Path="/6044242D/604458C7" Ref="#PWR0154"  Part="1" 
F 0 "#PWR0154" H 5550 5000 50  0001 C CNN
F 1 "GND" H 5555 5077 50  0000 C CNN
F 2 "" H 5550 5250 50  0001 C CNN
F 3 "" H 5550 5250 50  0001 C CNN
	1    5550 5250
	1    0    0    -1  
$EndComp
Text HLabel 3350 5000 0    50   Output ~ 0
32.768Hz_osc_in
Text HLabel 3350 5300 0    50   Input ~ 0
32.768Hz_osc_out
Wire Wire Line
	3350 5300 4400 5300
Wire Wire Line
	3350 5000 4400 5000
Wire Wire Line
	5550 5250 5550 5200
$Comp
L Device:Crystal Y?
U 1 1 604458D2
P 4400 5150
AR Path="/60440B15/604458D2" Ref="Y?"  Part="1" 
AR Path="/6044242D/604458D2" Ref="Y2"  Part="1" 
F 0 "Y2" V 4354 5281 50  0000 L CNN
F 1 "Resonator 32.768" V 4445 5281 50  0000 L CNN
F 2 "Crystal:Crystal_DS10_D1.0mm_L4.3mm_Horizontal" H 4400 5150 50  0001 C CNN
F 3 "~" H 4400 5150 50  0001 C CNN
	1    4400 5150
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 604458D8
P 4850 4900
AR Path="/60440B15/604458D8" Ref="C?"  Part="1" 
AR Path="/6044242D/604458D8" Ref="C19"  Part="1" 
F 0 "C19" V 4598 4900 50  0000 C CNN
F 1 "6pF" V 4689 4900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4888 4750 50  0001 C CNN
F 3 "~" H 4850 4900 50  0001 C CNN
	1    4850 4900
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 604458DE
P 4850 5500
AR Path="/60440B15/604458DE" Ref="C?"  Part="1" 
AR Path="/6044242D/604458DE" Ref="C20"  Part="1" 
F 0 "C20" V 5000 5500 50  0000 C CNN
F 1 "6pF" V 5100 5500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4888 5350 50  0001 C CNN
F 3 "~" H 4850 5500 50  0001 C CNN
	1    4850 5500
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 5000 4400 4900
Wire Wire Line
	4400 4900 4700 4900
Connection ~ 4400 5000
Wire Wire Line
	4400 5300 4400 5500
Wire Wire Line
	4400 5500 4700 5500
Connection ~ 4400 5300
Wire Wire Line
	5000 5500 5400 5500
Wire Wire Line
	5400 5500 5400 5200
Wire Wire Line
	5000 4900 5400 4900
Wire Wire Line
	5400 4900 5400 5200
Connection ~ 5400 5200
$EndSCHEMATC
