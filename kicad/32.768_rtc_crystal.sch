EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5850 4500 6000 4500
$Comp
L power:GND #PWR?
U 1 1 60448704
P 6000 4550
AR Path="/60448704" Ref="#PWR?"  Part="1" 
AR Path="/60440B15/60448704" Ref="#PWR0153"  Part="1" 
F 0 "#PWR0153" H 6000 4300 50  0001 C CNN
F 1 "GND" H 6005 4377 50  0000 C CNN
F 2 "" H 6000 4550 50  0001 C CNN
F 3 "" H 6000 4550 50  0001 C CNN
	1    6000 4550
	1    0    0    -1  
$EndComp
Text HLabel 3800 4300 0    50   Output ~ 0
osc_in
Text HLabel 3800 4600 0    50   Input ~ 0
osc_out
Wire Wire Line
	3800 4600 4850 4600
Wire Wire Line
	3800 4300 4850 4300
Wire Wire Line
	6000 4550 6000 4500
$Comp
L Device:Crystal Y2
U 1 1 604497AC
P 4850 4450
F 0 "Y2" V 4804 4581 50  0000 L CNN
F 1 "Resonator 32.768" V 4895 4581 50  0000 L CNN
F 2 "Crystal:Crystal_DS10_D1.0mm_L4.3mm_Horizontal" H 4850 4450 50  0001 C CNN
F 3 "~" H 4850 4450 50  0001 C CNN
	1    4850 4450
	0    1    1    0   
$EndComp
$Comp
L Device:C C19
U 1 1 6044A43D
P 5300 4200
F 0 "C19" V 5048 4200 50  0000 C CNN
F 1 "6pF" V 5139 4200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5338 4050 50  0001 C CNN
F 3 "~" H 5300 4200 50  0001 C CNN
	1    5300 4200
	0    1    1    0   
$EndComp
$Comp
L Device:C C20
U 1 1 6044AB9D
P 5300 4800
F 0 "C20" V 5450 4800 50  0000 C CNN
F 1 "6pF" V 5550 4800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5338 4650 50  0001 C CNN
F 3 "~" H 5300 4800 50  0001 C CNN
	1    5300 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	4850 4300 4850 4200
Wire Wire Line
	4850 4200 5150 4200
Connection ~ 4850 4300
Wire Wire Line
	4850 4600 4850 4800
Wire Wire Line
	4850 4800 5150 4800
Connection ~ 4850 4600
Wire Wire Line
	5450 4800 5850 4800
Wire Wire Line
	5850 4800 5850 4500
Wire Wire Line
	5450 4200 5850 4200
Wire Wire Line
	5850 4200 5850 4500
Connection ~ 5850 4500
$EndSCHEMATC
