EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Crystal Y?
U 1 1 604551F1
P 4800 4550
AR Path="/604551F1" Ref="Y?"  Part="1" 
AR Path="/60440CE4/604551F1" Ref="Y3"  Part="1" 
F 0 "Y3" V 4846 4419 50  0000 R CNN
F 1 "8Mhz" V 4755 4419 50  0000 R CNN
F 2 "Crystal:Crystal_HC18-U_Vertical" H 4800 4550 50  0001 C CNN
F 3 "~" H 4800 4550 50  0001 C CNN
	1    4800 4550
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C?
U 1 1 604551F7
P 5150 4700
AR Path="/604551F7" Ref="C?"  Part="1" 
AR Path="/60440CE4/604551F7" Ref="C22"  Part="1" 
F 0 "C22" V 4898 4700 50  0000 C CNN
F 1 "20pF" V 4989 4700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5188 4550 50  0001 C CNN
F 3 "~" H 5150 4700 50  0001 C CNN
	1    5150 4700
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 604551FD
P 5150 4350
AR Path="/604551FD" Ref="C?"  Part="1" 
AR Path="/60440CE4/604551FD" Ref="C21"  Part="1" 
F 0 "C21" V 5402 4350 50  0000 C CNN
F 1 "20pF" V 5311 4350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5188 4200 50  0001 C CNN
F 3 "~" H 5150 4350 50  0001 C CNN
	1    5150 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5000 4700 4800 4700
Wire Wire Line
	5000 4350 4800 4350
Wire Wire Line
	4800 4350 4800 4400
Wire Wire Line
	3600 4600 4550 4600
Wire Wire Line
	4550 4600 4550 4700
Wire Wire Line
	4550 4700 4800 4700
Connection ~ 4800 4700
Wire Wire Line
	4800 4350 4550 4350
Wire Wire Line
	4550 4350 4550 4500
Connection ~ 4800 4350
Wire Wire Line
	4550 4500 3600 4500
Wire Wire Line
	5300 4700 5300 4500
Wire Wire Line
	5500 4500 5300 4500
Connection ~ 5300 4500
Wire Wire Line
	5300 4500 5300 4350
Wire Wire Line
	5500 4500 5500 4450
$Comp
L power:GND #PWR?
U 1 1 60455213
P 5500 4450
AR Path="/60455213" Ref="#PWR?"  Part="1" 
AR Path="/60440CE4/60455213" Ref="#PWR0154"  Part="1" 
F 0 "#PWR0154" H 5500 4200 50  0001 C CNN
F 1 "GND" H 5505 4277 50  0000 C CNN
F 2 "" H 5500 4450 50  0001 C CNN
F 3 "" H 5500 4450 50  0001 C CNN
	1    5500 4450
	-1   0    0    1   
$EndComp
Text HLabel 3600 4500 0    50   Output ~ 0
osc_in
Text HLabel 3600 4600 0    50   Input ~ 0
osc_out
$EndSCHEMATC
