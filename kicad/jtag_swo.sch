EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 10
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3850 4300 2    50   BiDi ~ 0
swdio
Text HLabel 3550 4300 0    50   Output ~ 0
swclk
$Comp
L Connector:Conn_01x02_Male J4
U 1 1 604CD2A9
P 3650 4550
F 0 "J4" V 3804 4362 50  0000 R CNN
F 1 "Conn_01x02_Male" V 3713 4362 50  0000 R CNN
F 2 "" H 3650 4550 50  0001 C CNN
F 3 "~" H 3650 4550 50  0001 C CNN
	1    3650 4550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3750 4350 3750 4300
Wire Wire Line
	3750 4300 3850 4300
Wire Wire Line
	3650 4350 3650 4300
Wire Wire Line
	3650 4300 3550 4300
$EndSCHEMATC
